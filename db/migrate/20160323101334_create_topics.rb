class CreateTopics < ActiveRecord::Migration[5.0]
  def change
    create_table :topics do |t|
      t.string :name
      t.string :slug
      t.boolean :published, default: true
      
      t.index :name
    end
  end
end
