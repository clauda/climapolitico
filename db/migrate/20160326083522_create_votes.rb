class CreateVotes < ActiveRecord::Migration[5.0]
  def change
    create_table :votes do |t|
      t.references :question, foreign_key: true, type: :uuid
      t.references :option, foreign_key: true
      t.references :user, foreign_key: true
      t.boolean :anonymous, default: false

      t.timestamps
    end
  end
end
