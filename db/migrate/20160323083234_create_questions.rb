class CreateQuestions < ActiveRecord::Migration[5.0]
  def change
    execute "CREATE EXTENSION IF NOT EXISTS hstore"
    execute "CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\""

    create_table :questions, id: :uuid, default: "uuid_generate_v4()", force: true do |t|
      # t.uuid :uuid, default: "uuid_generate_v4()"
      t.text :title
      t.boolean :published, default: false
      t.boolean :closed, default: false
      t.boolean :accepted, default: false
      t.boolean :anonymous, default: false
      t.boolean :featured, default: false
      t.references :user, foreign_key: true
      t.string :status, default: "awaiting_moderation"
      t.string :rejection_reason
      t.string :kinda, default: "yes_no" # Type
      t.integer :votes_count, default: 0
      t.string :slug
      t.hstore :meta
      
      t.index :title, unique: true

      t.timestamps
    end
  end
end
