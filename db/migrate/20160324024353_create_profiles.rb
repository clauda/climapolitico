class CreateProfiles < ActiveRecord::Migration[5.0]
  def change
    create_table :profiles do |t|
      t.references :user, foreign_key: true
      t.string :name
      t.string :image_url
      t.boolean :public, default: false

      # Questionário Sócio Econômico
      t.string :genre
      t.string :state
      t.string :age
      t.string :income
      t.string :schooling
      t.string :race
      t.string :political_leanings

      t.timestamps
    end
  end
end
