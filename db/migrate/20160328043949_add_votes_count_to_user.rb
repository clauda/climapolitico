class AddVotesCountToUser < ActiveRecord::Migration[5.0]
  def up
    add_column :users, :votes_count, :integer
  end
  
  def down
    remove_column :users, :votes_count
  end
end
