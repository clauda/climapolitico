class AddTopicToQuestion < ActiveRecord::Migration[5.0]
  def up
    add_column :questions, :topic_id, :integer, index: true, references: :topic
    add_column :topics, :questions_count, :integer, default: 0
  end

  def down
    remove_column :questions, :topic_id
    remove_column :topics, :questions_count
  end
end
