class CreateOptions < ActiveRecord::Migration[5.0]
  def change
    create_table :options do |t|
      t.references :question, foreign_key: true, type: :uuid
      t.integer :votes_count, default: 0
      t.string :name
    end
  end
end
