# The Climão

Admin.create(name: 'Administrador', email: 'admin@climapolitico.com.br', 
    password: 'q1w2e3r4', password_confirmation: 'q1w2e3r4', role: 'god')
Topic.create(name: 'Limbo', published: false)

['Eleições Presidenciais 2018',
 'Golpe Militar',
 'Maioridade Penal',
 'Operação Lava Jato',
 'Casamento Igualitário',
 'Homossexuais',
 'Direitos Humanos'].map { |topic| Topic.create(name: topic) }

user = User.create(email: 'claudia@sorta.in', password: 'q1w2e3r4', password_confirmation: 'q1w2e3r4')
user.profile.name = "Claudia Farias"
user.profile.state = "SP"
user.profile.save
# Questions
# Bruna
# 1. Você é a favor da distribuição da cartilha 'O Aparelho Sexual e Cia.' 
#   nas escolas feita pelo MEC?
# 2. Você concorda com a ideologia de gênero?
# 3. Você é a favor da liberação de armas?

# Claudia
Question.create(title: 'Você é a favor do impeachment da Presidenta Dilma?', topic: Topic.find(2), featured: true, user: user);
Question.create(title: 'Lula deve se tornar ministro da Casa Civil?', topic: Topic.find(2), user: user);
Question.create(title: 'Você votaria no Deputado Jair Bolsonaro (PCP/RJ) na campanha presidencial de 2018?', topic: Topic.find(2), user: user);
# 4. Você concorda com a divulgação das gravações feitas entre Dilma e Lula pelo Juiz Moro?
# 5. Bandido bom é bandido morto?
# 6. Você é defende a volta da ditatura militar?
# 7. Você é a favor do Casamento Civil igualitário entre pessoas do mesmo sexo?
# 8. Você é a favor da maioridade penal de 16 anos?
# 9. A Globo tem um jornalismo sério e imparcial. Você concorda com esta afirmação?
# 10. Você é a favor do aborto?
# 11. Você é a favor da liberação do uso da maconha?
