## O CLIMA POLITICO

Uma forma de medir a temperatura dos animos no setor político.

### Configuration

* Ruby 2.3.0

* Rails 5

* Postgres

### Setup

`rake db:setup`

`rails server`

### TODO

* Docker Container
* Moderar Perguntas
* Gráficos com Resultados
* Perfil (incluir dados socioeconomicos (opcional))
* carousel de perguntas
* Reportar Erro