# Where the I18n library should search for translation files
# I18n.load_path += Dir[Rails.root.join('config', 'locale', '*.{rb,yml}')]
 
# Set default locale to something other than :en
# config.i18n.locale = :'pt-BR' 
# I18n.default_locale = :'pt-BR'