Rails.application.routes.draw do

  devise_for :users, 
    path: 'acesso', 
    path_names: { 
      sign_in: 'login', 
      sign_out: 'sair', 
      password: 'recuperar', 
      confirmation: 'confirme', 
      unlock: 'desbloquear',
      registration: 'cadastro', 
      sign_up: 'participar' 
    }, controllers: { 
      omniauth_callbacks: 'users/omniauth_callbacks' 
    }

  get '/politica-de-privacidade', to: 'frontend#privacy'
  get '/sitemaps', to: 'sitemaps#index'
  get '/painel', to: 'questions#index', as: :painel
  get '/painel/surpresa', to: 'questions#randomic', as: :random
  resources :topics, only: [:index, :show], path: 'temas'
  scope(only: [:index, :new, :create, :show], path_names: { new: 'fabrica' }) do
    resources :questions, path: 'vote' do
      get :results, to: :results, as: :results, on: :member, path: 'resultados'
      match :vote, to: :vote, on: :member, via: [:post, :patch]
    end
  end

  devise_for :admins, path: 'admin'
  namespace :admin do
    resources :topics, only: [:index, :create, :update]
    resources :questions do
      patch :approve, to: :approve, on: :member
      patch :reject, to: :reject, on: :member
    end
    root to: 'dashboard#index'
  end

  resources :profiles, only: [:update]
  get '/perfil/:token/editar', to: 'profiles#edit', as: :account_edit
  get '/perfil(/:token)', to: 'profiles#index', as: :account

  root to: 'frontend#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # Serve websocket cable requests in-process
  # mount ActionCable.server => '/cable'
end
