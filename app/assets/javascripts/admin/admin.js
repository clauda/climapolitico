// This is a manifest file that'll be compiled into application, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .

//= require admin/lib/modernizr
//= require common/bootstrap
//= require admin/lib/jquery.easing
//= require admin/lib/fastclick
//= require admin/lib/jquery.onscreen
//= require admin/lib/jquery.countTo
//= require admin/lib/perfect-scrollbar.jquery
//= require admin/ui/accordion
//= require admin/ui/animate
//= require admin/ui/link-transition
//= require admin/ui/panel-controls
//= require admin/ui/preloader
//= require admin/ui/toggle
//= require admin/lib/lib
