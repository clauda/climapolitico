class ProfilesController < ApplicationController
  before_action :authenticate_user!, only: [:edit, :update]
  before_action :retrieve

  def index
    @questions = @profile.user.questions
  end

  def edit
    @questions = @profile.user.questions
  end

  def update
    if @profile.update(profile_params)
      redirect_to account_path(@profile.token), notice: 'Seus dados foram atualizados'
    else
      render :edit
    end
  end

  protected

    def retrieve
      @profile = params[:token] ? Profile.find_by(token: params[:token]) : current_user.profile
      denied!
    end

    def denied!
      if current_user
        if @profile.user_id != current_user.id
          redirect_to questions_path, notice: 'Negado'
          return 
        end
      else
        redirect_to root_path
      end
    end

    def profile_params
      params.require(:profile).permit(:name, :public, :genre, :age, :state, :race, :schooling, :political_leanings, :income)
    end

end
