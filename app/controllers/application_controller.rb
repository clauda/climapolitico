class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  layout :leiaute

  def after_sign_out_path_for resource_or_scope
    request.referrer
  end

  protected

    def leiaute
      if devise_controller?
        resource.is_a?(Admin) ? 'auth' : 'application'
      else
        'application'
      end
    end

  private

    def after_sign_in_path_for resource
      session["user_return_to"] || root_path
    end

end
