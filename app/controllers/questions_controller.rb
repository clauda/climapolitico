class QuestionsController < ApplicationController
  before_action :authenticate_user!, only: [:index, :new, :create, :vote]
  before_action :retrieve, only: [:show, :results, :vote]

  def index
    @questions = current_user.questions.all
    redirect_to new_question_path if @questions.empty?
  end

  def new
    @question = Question.new
  end

  def show
    @question.increment_view!
    @vote = current_user ? current_user.votes.find_or_initialize_by(question_id: @question.id) : Vote.new
  end

  def create
    @question = Question.find_by(title: question_params['title'])
    if @question
      redirect_to :back, alert: 'Opa, parece que alguém já fez esta pergunta. Que tal outra?'
    else
      @question = current_user.questions.new(question_params)
      if @question.save(validate: false)
        redirect_to questions_path, notice: 'Sua pergunta foi enviada e está aguardando moderação.'
      else
        redirect_to :back, error: 'Ops'
      end
    end
  end

  def randomic
    # TODO: exclude asked questions
    question = Question.active.order('RANDOM()').first
    redirect_to question_path(question)
  end

  def vote
    @vote = current_user.votes.find_or_create_by(question_id: @question.id)
    option = @question.options.find(params[:vote][:option])
    if option
      @vote.option_id = option.id
      @vote.anonymous = params[:vote][:anonymous]
      @vote.save
    end
    redirect_to random_path
  end

  private

    # Only allow a trusted parameter "white list" through.
    def question_params
      params.require(:question).permit(:title, :anonymous)
    end

    def retrieve
      @question = Question.by_slug(params[:id])
    end
end
