class TopicsController < ApplicationController

  def index
    @topics = Topic.active.all
  end

  def show
    @topic = Topic.by_slug(params[:id])
    @questions = @topic.questions
  end

end
