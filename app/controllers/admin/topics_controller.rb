class Admin::TopicsController < Admin::AdminController

  def index
    @topics = Topic.all
  end

  def create
    @topic = Topic.new(topic_params)
    if @topic.save
      redirect_to admin_topics_path, notice: 'Tema was successfully created.'
    end
  end

  def update
    @topic = Topic.by_slug(params[:id])
    @topic.toggle!(:published)
    redirect_to :back, notice: 'Ok, done.'
  end

  private

    def topic_params
      params.require(:topic).permit(:name)
    end
end
