class Admin::QuestionsController < Admin::AdminController
  before_action :set_question, only: [:show, :edit, :update, :destroy, :approve, :reject]

  # GET /questions
  def index
    @questions = Question.all
  end

  # GET /questions/1
  def show
  end

  # GET /questions/new
  def new
    @question = Question.new
  end

  # GET /questions/1/edit
  def edit
  end

  # POST /questions
  def create
    @question = Question.new(question_params)

    if @question.save
      redirect_to admin_question_path(@question), notice: 'Question was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /questions/1
  def update
    @question.title = question_params[:title]
    old_title = @question.changed_attributes[:title] if @question.changed_attributes.include?(:title)
    if @question.update(question_params)
      @question.create_activity(key: 'question.title_updated', owner: current_admin, parameters: { title: old_title }) if old_title
      @question.create_activity key: 'question.updated', owner: current_admin
      redirect_to admin_question_path(@question), notice: 'Question was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /questions/1
  def destroy
    @question.destroy
    redirect_to admin_questions_url, notice: 'Question was successfully destroyed.'
  end

  def approve
    @question.update({ published: true, accepted: true, status: 'approved_published', topic_id: question_params[:topic_id] })
    @question.log_activities("question.approved_published")
    redirect_to :back, notice: 'Aprovada e Publicada!'
  end

  def reject
    @question.update({ published: false, accepted: false, status: 'rejected', rejection_reason: question_params[:rejection_reason] })
    @question.log_activities("question.rejected")
    redirect_to :back, warn: 'Rejeitada!'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_question
      @question = Question.by_slug(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def question_params
      params.require(:question).permit(:title, :published, :closed, :user_id, :topic_id, :anonymous, :accepted, :rejection_reason)
    end
end
