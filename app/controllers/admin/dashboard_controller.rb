class Admin::DashboardController < Admin::AdminController
  def index
    @votes = Vote.all.size
    @questions = Question.all.size
    @published = Question.active.size
    @waiting = Question.await.size
    @users = User.all.size
  end
end
