class SitemapsController < ApplicationController
  respond_to :xml

  def index
    @questions = Question.active.all.order("created_at DESC")
    @topics = Topic.active.all
  end

end
