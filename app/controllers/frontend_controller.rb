class FrontendController < ApplicationController
  layout 'home'

  def index
    @spotlight = ENV['QID'] ? Question.find(ENV['QID']) : Question.spotlight.first
    @questions = Question.active.limit(6)
    @topics = Topic.active.order("RANDOM()").limit(6)
  end

  def privacy
    render layout: 'application'
  end
  
end
