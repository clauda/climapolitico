class Profile < ApplicationRecord
  belongs_to :user
  
  delegate :email, to: :user

  before_create :setoken

  def setoken
    self.token = tokenize
  end

  protected

    def tokenize
      loop do
        token = SecureRandom.base64(6)
        break token unless self.class.find_by(token: token)
      end
    end
end
