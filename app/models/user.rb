class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and 
  devise :database_authenticatable, :registerable, :omniauthable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :questions
  has_many :votes
  has_one :profile

  after_create :la_persona

  def self.from_omniauth auth
    passwrd = Devise.friendly_token[0,20]
    
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email
      user.password = passwrd
      user.password_confirmation = passwrd
      user.save

      profile = Profile.find_or_create_by(user: user)
      profile.name = auth.info.name
      profile.image_url = auth.info.image
      profile.save
    end
  end

  private

    def la_persona
      profile = Profile.find_or_create_by(user: self)
    end

end
