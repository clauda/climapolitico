module Sluggable
  extend ActiveSupport::Concern

  included do
    before_create :slug_me
  end

  def self.included base
    base.class_eval do

      def self.by_slug id
        find_by(slug: id) if id
      end
    end
  end

  def to_param
    self.slug
  end

  def slug_me
    self.slug = self.title.parameterize
  end
end