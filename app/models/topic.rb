class Topic < ApplicationRecord
  include Sluggable
  alias_attribute :title, :name
  
  has_many :questions
  validates :name, presence: true

  scope :active, -> { where(published: true) }
end
