class Vote < ApplicationRecord
  belongs_to :question, counter_cache: true
  belongs_to :option, counter_cache: true
  belongs_to :user, counter_cache: true

  scope :anonymo, -> { where(anonymous: true) }
end
