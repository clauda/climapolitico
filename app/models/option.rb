class Option < ApplicationRecord
  belongs_to :question
  has_many :votes
  validates :name, presence: true
end
