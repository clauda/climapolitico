class Question < ApplicationRecord
  include PublicActivity::Common
  include Sluggable

  STATUSES = {
    awaiting_moderation: 'Aguardando Aprovação',
    approved: 'Aprovada',
    approved_published: 'Aprovada e Publicada',
    rejected: 'Pergunta Rejeitada'
  }

  REJECTION_REASONS = {
    repeated: 'Pergunta Repetida',
    no_politics: 'Pergunta foge ao tema político',
    irrelevant: 'Considerada Irrevelante'
  }
  
  belongs_to :topic, counter_cache: true
  belongs_to :user
  has_many :options, dependent: :delete_all
  has_many :votes

  validates :title, presence: true

  store_accessor :meta, :views
  store_accessor :meta, :image_url

  after_create :setup!

  scope :active, -> { where(published: true) }
  scope :await, -> { where(status: :awaiting_moderation) }
  scope :spotlight, -> { active.where(featured: true) }
  scope :anonymo, -> { active.where(anonymous: true) }

  def views
    self.meta['views'] || 0
  end

  def image_url
    self.meta['image_url']
  end

  def increment_view!
    self.meta = {} unless self.meta
    views = self.meta['views']
    self.meta['views'] = views ? views.to_i + 1 : 1
    self.save
  end

  def log_activities activity_key
    self.create_activity key: activity_key, owner: self.user if activity_key
  end

  def rejected?
    self.status == 'rejected'
  end

  protected

    def setup!
      create_yes_no_options # Create Options
      # Log Activities
      log_activities(self.anonymous ? "question.submit_anonymous" : "question.submit_public")
      # Setup Metadata
      self.update_column(:meta, { views: 0, image_url: nil })
    end

    # Its prepared to multiple choices
    def create_yes_no_options
      if self.options.empty? && self.kinda == 'yes_no'
        ['Sim', 'Não'].map { |choice| self.options.create(name: choice) }
      end
    end
end
