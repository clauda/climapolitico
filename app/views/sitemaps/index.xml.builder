xml.instruct!

xml.urlset(xmlns: "http://www.sitemaps.org/schemas/sitemap/0.9") do
  xml.url do
    xml.loc root_url
    xml.changefreq("hourly")
    xml.priority "1.0"
  end
  
  @questions.each do |resource|  
    xml.url do
      xml.loc url(question_path(resource))
      xml.changefreq("daily")
      xml.priority "0.8"
      xml.lastmod resource.updated_at.strftime("%Y-%m-%dT%H:%M:%S.%2N%:z")
    end
  end

  @topics.each do |resource|  
    xml.url do
      xml.loc url(topic_path(resource))
      xml.changefreq("weekly")
      xml.priority "0.8"
    end
  end
end