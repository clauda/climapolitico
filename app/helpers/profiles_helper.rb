module ProfilesHelper

  GENRES = [ 'Masculino', 'Feminino' ]

  AGE_RANGE = [
    'Menor de 12 anos',
    'Entre 12 e 17 anos',
    'Entre 18 e 24 anos',
    'Entre 25 e 34 anos',
    'Entre 35 e 44 anos',
    'Entre 45 e 54 anos',
    'Entre 55 e 64 anos',
    'Entre 65 e 74 anos',
    '75 anos ou mais'
  ]

  INCOME_RANGE = [
    'Até R$ 1.500,00',
    'Entre R$ 1.500,00 até R$ 3.000,00.',
    'Entre R$ 3.000,00 até R$ 6.000,00.',
    'Acima de R$ 6.000,00',
    'Não possui nenhuma renda'
  ]

  SCHOOLINGS = [
    'Primeiro Grau Incompleto',
    'Primeiro Grau Completo',
    'Segundo Grau Incompleto',
    'Segundo Grau Completo',
    'Curso Técnico',
    'Superior Incompleto',
    'Superior Completo'
  ]

  RACES = [
    'Branco',
    'Pardo',
    'Preto',
    'Indígena',
    'Oriental'
  ]

  STATES = [ 
    ['Acre',  'AC'],
    ['Alagoas', 'AL'],
    ['Amapá', 'AP'],
    ['Amazonas', 'AM'],
    ['Bahia', 'BA'],
    ['Ceará', 'CE'],
    ['Distrito Federal', 'DF'],
    ['Espírito Santo', 'ES'],
    ['Goiás', 'GO'],
    ['Maranhão', 'MA'],
    ['Mato Grosso', 'MT'],
    ['Mato Grosso do Sul', 'MS'],
    ['Minas Gerais', 'MG'],
    ['Pará', 'PA'],
    ['Paraíba', 'PB'],
    ['Paraná', 'PR'],
    ['Pernambuco', 'PE'],
    ['Piauí', 'PI'],
    ['Rio de Janeiro', 'RJ'],
    ['Rio Grande do Norte', 'RN'],
    ['Rio Grande do Sul', 'RS'],
    ['Rondônia', 'RO'],
    ['Roraima', 'RR'],
    ['Santa Catarina', 'SC'],
    ['São Paulo', 'SP'],
    ['Sergipe', 'SE'],
    ['Tocantins', 'TO']
  ]

  POLITICALS = ['Nenhuma', 'Direita', 'Esquerda']

  def is_current_user? profile
    current_user && current_user.profile == profile
  end

  def image_for profile
    unless profile.user.provider
      hash = Digest::MD5.hexdigest(profile.email)
      image_tag("http://www.gravatar.com/avatar/#{hash}")
    else
      image_tag(profile.image_url)
    end
  end

end
